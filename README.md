# Keypoint Detection

This web demo uses a model based on convolution neural network to automatically 
detect keypoints in a photograph of a human body. More details are available [here](http://www.robots.ox.ac.uk/~vgg/software/keypoint_detection/).

## Deploying the webapp to vggdebug2

To compile matconvnet:
```
adutta@vggdebug2:/data/adutta/vggdemo/keypoint_webapp/app/static/matlab/lib/matconvnet-custom$ matlab -nodesktop -nosplash
> addpath matlab
> vl_compilenn('enableGpu', true)
```

To run the webapp:

```
$ cd /data/adutta/vggdemo/keypoint_webapp
$ source /data/adutta/mybin/virtualenv-15.0.3/webdemo/bin/activate
$ (webdemo) adutta@vggdebug2:/data/adutta/vggdemo/keypoint_webapp$ emacs config.py #
$ (webdemo) adutta@vggdebug2:/data/adutta/vggdemo/keypoint_webapp$ python run.py
```


For further details, contact [Abhishek Dutta](mailto:adutta@robots.ox.ac.uk)
Aug. 23, 2016