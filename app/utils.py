import os.path as osp
from werkzeug import secure_filename
from app import app
import string
import random
import urllib2
import socket
import json

def id_generator(size=20,chars=string.ascii_uppercase + string.digits):
  """
  return a random string:
  """
  return ''.join(random.choice(chars) for _ in range(size))

def get_random_file_name(fname):
  """
  return a random filename for saving the uploads:
  """
  _,ext = osp.splitext(fname)
  filename = id_generator() + ext;
  filename = secure_filename(filename)
  filename = osp.join(app.config['UPLOAD_FOLDER'], filename)
  return filename

def get_reponse_string(success, text):
  return '{"success":%d,"msg":\"%s\"}'%(success,text)

class FileSizeException(Exception):
    pass

def download_file(url,save_fname,max_size):
  f = urllib2.urlopen(url, timeout=5)
  f = f.read(max_size+1)
  if len(f) > max_size:
    raise FileSizeException('file size too big.')
  else:
    # save the file: 
    with open(save_fname,'wb') as im:
      im.write(f)
  
