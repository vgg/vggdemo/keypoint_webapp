var uploadmsg_string = '';

function reset_form_element(e) {
  e.wrap('<form>').closest('form').get(0).reset();
  e.unwrap();
}

function isvalid_image(url, success_cb, error_cb) {
  $img = $("<img>", {src: url,
                     error: error_cb,
                     load: function(){success_cb($img);}
                     })  ;
}

function update_msg(str, color) {
  if (typeof(color) === 'undefined') {
    color = '#555';
  }
  $('#upload_msg').text(str);
  $('#upload_msg').css('color', color);
}
function flash_message(msg,color,t) {
  update_msg(msg,color);
  setTimeout(function(){update_msg(uploadmsg_string);}, t);
}

function read_local_file(file) {
  if (file.type.match('image/png') || file.type.match('image/jpeg')) {
    var reader = new FileReader(file);
    reader.readAsDataURL(file);
    reader.onload = function(e) {
      // nullify urlfield:
      $('#urlfield').val('');
      var data = e.target.result,
      $img = $('<img />').attr('src', data).fadeIn();
      $('#dropim div').html($img);
      $('#go_btn_div').animate({width:'show'},300);
    };
  }
}

var typewatch = function(callback, ms){
  var tim = 0;
  return function(){
    clearTimeout (tim);
    tim = setTimeout(callback, ms);
  }  
};

function clear_image_area() {
  $('#dropim img').remove();
  $('#dropim').removeClass('hover');
  $('#dropim').removeClass('dropped');
  $('#spinner').hide();
  $('#go_btn_div').animate({width:'hide'},300);  
}

var url_input_cb = function() {
  if (typeof url_input_cb.prev_val == 'undefined') url_input_cb.prev_val = '';
  if (url_input_cb.prev_val == $('#urlfield').val()) return;
  url_input_cb.prev_val = $('#urlfield').val();
  var in_url = $('#urlfield').val();
  isvalid_image(in_url,
                function(img) {
                  // nullify fileinput:
                  var $fi = $('#fileinput');
                  reset_form_element($fi);

                  $('#dropim').addClass('dropped');
                  $('#dropim img').remove();
                  $('#dropim div').html(img);
                  $('#go_btn_div').animate({width:'show'},300);
                }, // success callback -- valid input URL
                function(){
                  clear_image_area();
                  if (in_url.length > 0) {
                    flash_message('Invalid image URL!','#F66',700);
                  } else {
                    update_msg(uploadmsg_string);
                  }
                }); // error callback -- invalid input URL
};

function handle_response(data) {
  if (data['success']) {
    window.location.href = data['msg'];
  } else {
    clear_image_area();
    flash_message(data['msg'],'#FF6F6F',1500);    
  }
}

function send_process_request() { 
  $('#go_btn_div').hide();
  $('#spinner').show(); // show processing animation..       
  var formData = new FormData($('#query_form')[0]);
  $.ajax({
    type: "POST",
    url: $('#query_form').attr('action'),
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    dataType: 'json',
    success:  function(data) {
                handle_response(data);
              },
    statusCode: {502: function() { flash_message('Image size is too big (need < 10MB).','#FF6F6F',1000); } },
    error: function(xhr,status,error) {
              // var err = eval("(" + xhr.responseText + ")");
              // alert(err.Message);
              clear_image_area();
              flash_message('Something went wrong. Retry?','#FF6F6F',1000);
            }
  });
}

function update_viewer(e) {
  var file;
  try {
    file = e.originalEvent.dataTransfer.files;
    if ((file != null) && (file.length > 0)) {
      file = file[0];
    } else {
      throw "invalid";
    }
  } catch(err) {
    try {
      file = this.files;
      if ((file != null) && (file.length > 0)) {
        file = file[0];
      } else { throw "invalid"; }
    } catch(err) {
      try {
        file = e.originalEvent.dataTransfer.getData('text/html').match(/src\s*=\s*"(.+?)"/)[1];
        isvalid_image(file,
          function(img) {
            // nullify fileinput:
            var $fi = $('#fileinput');
            reset_form_element($fi);
            $('#urlfield').val(file);
            $('#dropim').addClass('dropped');
            $('#dropim img').remove();
            $('#dropim div').html(img);
            $('#go_btn_div').animate({width:'show'},300);
          }, // success callback -- valid input URL
          function(){
            clear_image_area();
            if (in_url.length > 0) {
              flash_message('Invalid image URL!','#F66',700);
            } else {
              update_msg(uploadmsg_string);
            }
          }); // error callback -- invalid input URL
        return;
      } catch (err) {
        $('#dropim').removeClass('hover');
        flash_message('Try some other image!','#F66',1000);
        return;
      }
    }
  }

  $('#dropim').removeClass('hover');

  if (this.accept && $.inArray(file.type, this.accept.split(/, ?/)) == -1) {
    flash_message('Can only munch png/jpg images!','#F66',1000);
    return;
  }
  $('#dropim').addClass('dropped');
  $('#dropim img').remove();
  read_local_file(file);
}

$(function() {

  uploadmsg_string = $('#upload_msg').html();

  $('#urlfield').on( {keyup: typewatch(url_input_cb,300)} );

  $('#cancel_btn').on({click: function() {
                                url_input_cb.prev_val = '';
                                $('#urlfield').val('');
                                clear_image_area();
                                update_msg(uploadmsg_string);
                              }
                      });

  $('#query_form').on('submit', function(e){e.preventDefault(); send_process_request();});
  $('#go_btn').on({click: send_process_request});

  $('#dropim').on('dragover', function(e) {
    e.stopPropagation();
    e.preventDefault();
    $('#dropim').removeClass('dropped');
    $(this).addClass('hover');
    update_msg('Upload');
  });

  $('#dropim').on('dragleave', function() {
    clear_image_area();
    //$(this).removeClass('hover');
    update_msg(uploadmsg_string);
  });

  $('#dropim input').on('drop', update_viewer);
  $('#dropim input').on('change', update_viewer);
});