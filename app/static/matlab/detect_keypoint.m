function keypoint_data = detect_keypoint(KPD, im_fname)
  fprintf(1, '\nDetecting keypoints for image = ');
  disp(im_fname);

  [kpx, kpy, kpname] = KPD.get_all_keypoints(im_fname);
  keypoint_data = res2json(kpx, kpy, kpname, im_fname, KPD.model_name, KPD.model_version);
end

function json = res2json(kpx, kpy, kpname, im_fname, model_name, model_version)
  dat = struct();
  [~,fn,ext] = fileparts(im_fname);
  dat.model_name = model_name;
  dat.model_version = model_version;
  dat.keypoint_count = length(kpname);
  dat.keypoint_x = kpx;
  dat.keypoint_y = kpy;
  dat.keypoint_name = kpname;
  json = savejson('',dat);
end
