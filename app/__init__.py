from flask import Flask
import matlab_bridge

# create the app:
app = Flask(__name__)

# config the APP:
app.config.from_object('config')

# create the matlab backend object:
app.backend = matlab_bridge.KeyPointDetectionBackend(app.config)

# add a file-logger:
if not app.debug:
  import logging
  from logging.handlers import RotatingFileHandler
  h = RotatingFileHandler('app/static/keypoint.log', maxBytes=100000, backupCount=1)
  h.setLevel(logging.INFO)
  app.logger.addHandler(h)

# 'views' defines the main logic:
from app import views
