from flask import render_template, request, send_file, json, redirect
from app import app
import os.path as osp
import os
import utils
import json
import csv

def process_image(imname):
  json_str = app.backend.process(imname)
  
  if json_str != -1:
    # sucess, save the json result file:
    imname = osp.basename(imname)
    basename,_ = osp.splitext(imname)
    jsonname = basename + '.json'
    jsonname = osp.join(app.backend.config['JSON_DIR'], jsonname)
    with open(jsonname, 'w') as f:
      f.write(json_str)

    # write the text-only output:
    csvfn = basename + '.csv'
    csvfn = osp.join(app.backend.config['CSV_DIR'], csvfn)
    json_data = json.loads(json_str)
    if len(json_data['keypoint_x']) == len(json_data['keypoint_y']):
      csvf = csv.writer(open(csvfn, 'w'))
      csvf.writerow(['model_name',
                     'model_version',
                     'keypoint_name',
                     'keypoint_x',
                     'keypoint_y'])
      for i in range(0, len(json_data['keypoint_x'])):
        csvf.writerow([json_data['model_name'],
                       json_data['model_version'],
                       json_data['keypoint_name'][i],
                       json_data['keypoint_x'][i],
                       json_data['keypoint_y'][i]])
    else:
      print 'Ignoring csv file export as json data is malformed.'
      
    # build the response string:
    imname = osp.basename(imname)
    return utils.get_reponse_string(True, "result-im=%s"%imname)
  else:
    return utils.get_reponse_string(False, "Error in processing. Try some other image?")

# serve processed txt DOWNLOAD data:
@app.route('/csv-download/<path:filename>', methods=['GET'])
def serve_txt_download(filename):
  return send_file( osp.join(app.backend.config['CSV_DIR'],filename),
                    mimetype='application/plain',
                    attachment_filename='results.csv',
                    as_attachment=True )

# serve processed json DOWNLOAD data:
@app.route('/json-download/<path:filename>', methods=['GET'])
def serve_json_download(filename):
  return send_file( osp.join(app.backend.config['JSON_DIR'],filename),
                    mimetype='application/json',
                    attachment_filename='results.json',
                    as_attachment=True )


# serve processed json data:
@app.route('/json-res/<path:filename>', methods=['GET'])
def serve_json_result(filename):
  return send_file(osp.join(app.backend.config['JSON_DIR'],filename))

# serve uploaded image:
@app.route('/im/<path:filename>', methods=['GET'])
def serve_image(filename):
  return send_file(osp.join(app.backend.config['UPLOAD_FOLDER'],filename))

# serve the results pages:
@app.route('/result-im=<path:imname>', methods=['GET'])
def show_results(imname):
  basename,_ = osp.splitext(imname)
  jsonname = basename + '.json'
  csvname = basename + '.csv'
  model_version = app.backend.KPD.model_version
  return render_template('show_result.html', imname=imname, jsonname=jsonname, csvname=csvname, model_version=model_version)

@app.route('/index/', methods=['GET', 'POST'])
@app.route('/', methods=['GET', 'POST'])
def index():
  # if request.url[-1] != '/':
  #   return redirect(request.url + '/')
  if request.method == 'POST':
    if 'filef' in request.files: # file input from a dialog box
      dialog_file = request.files['filef']
      fname = dialog_file.filename
      app.logger.info('DIALOG file: ' + fname)
      local_fname = utils.get_random_file_name(fname)
      try:
        dialog_file.save(local_fname)
        app.logger.info(' -> file download SUCCESS!')
      except Exception, e:
        app.logger.warning(' -> file download FAILED!')
        app.logger.warning( str(e) )
        return utils.get_reponse_string(False, "Image download failed. Retry?")

    elif 'urlf' in request.form: # file given as a url
      url_file = request.form['urlf']
      app.logger.info('URL file: ' + url_file)
      local_fname = utils.get_random_file_name(url_file)
      try:
        utils.download_file(url_file,local_fname,app.config['MAX_CONTENT_LENGTH'])
        app.logger.info(' -> file download SUCESS!')
      except utils.FileSizeException:
        app.logger.warning(' -> file download FAILED: too big!')
        return utils.get_reponse_string(False, "Image size is too big (need < 10 MB).")        
      except:
        app.logger.warning(' -> file download FAILED')
        return utils.get_reponse_string(False, "Cannot download image from the URL!")
    else:
      return utils.get_reponse_string(False, "Image download failed!")

    #successful image download! Now, TRY to process and serve results:
    app.logger.info('processing file...')
    return process_image(local_fname)
  else:
    model_version = app.backend.KPD.model_version
    return render_template('index.html', model_version=model_version)

@app.route('/changelog', methods=['GET'])
def show_changelog():
  return send_file(app.backend.config['MODEL_BASE_DIR'] + 'changelog.txt')
