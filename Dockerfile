#
# Dockerfile for creating a docker image VGG demo: keypoint
#
# Author: Abhishek Dutta <adutta@robots.ox.ac.uk>
# Date: 13 July 2018
#

#
# See: https://hub.docker.com/r/nvidia/cuda/
#
FROM nvidia/cuda:8.0-devel-ubuntu16.04

#
# Define image labels
#
LABEL name="Keypoint Online Demo" codename="keypoint" version="latest" maintainer="Abhishek Dutta <adutta@robots.ox.ac.uk>" description="VGG Demo: keypoint"

EXPOSE 9001 9001

VOLUME /data/vgg/vggdemo/keypoint /usr/local/MATLAB/R2016a/

#
# Define environment variables
#
ENV GPU_ID="1"

#
# Compile dependencies (fastann, pypar, etc) and then compile VISE
#
RUN /bin/bash -c 'apt-get -y update && apt-get -y install python-dev git python-pip libjpeg62-dev joe ; \
 pip install flask numpy scipy ; \
 pip install git+https://github.com/cpbotha/mlabwrap-purepy.git ; \
 sed -i "s/mlab_command.__doc__/#mlab_command.__doc__/g" /usr/local/lib/python2.7/dist-packages/mlabwrap/mlabwrap.py ; \
 mkdir /opt/vgg/vggdemo/keypoint/code/ -p && cd /opt/vgg/vggdemo/keypoint/code ; \
 git clone https://gitlab.com/vggdemo/keypoint_webapp.git ; \
 cd /opt/vgg/vggdemo/keypoint/code/keypoint_webapp/app/static/matlab/lib/matconvnet-custom ; \
 /usr/local/MATLAB/R2016a/bin/matlab -nodesktop -nosplash -r "addpath matlab; vl_compilenn('enableGpu', true); exit"'

ENTRYPOINT LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libstdc++.so.6 python /data/vgg/vggdemo/keypoint/code/dev/keypoint_webapp/run.py

